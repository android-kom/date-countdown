package com.thefinalcountdown.komfushee.thefinalcountdown;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView CountDownDayTextView;
    TextView CountDownHoursTextView;
    TextView CountDownMinutesTextView;
    TextView CountDownSecondsTextView;
    ImageView CountDownImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CountDownDayTextView = findViewById(R.id.CountDownDayTextView);
        CountDownHoursTextView = findViewById(R.id.CountDownHoursTextView);
        CountDownMinutesTextView = findViewById(R.id.CountDownMinutesTextView);
        CountDownSecondsTextView = findViewById(R.id.CountDownSecondsTextView);
        CountDownImageView = findViewById(R.id.CountDownImageView);

        String givenDateString = "Mon Mar 19 00:00:00 GMT+01:00 2018";
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.getDefault());

        try {
            Date mDate = sdf.parse(givenDateString);

            long timeInMilliseconds = mDate.getTime();
            long millisLeft = timeInMilliseconds - System.currentTimeMillis();

            new CountDownTimer(millisLeft, 1000) {
                StringBuilder time = new StringBuilder();

                public void onTick(long millisUntilFinished) {
                    time.setLength(0);

                    if (millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
                        long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
                        CountDownDayTextView.setText(String.valueOf(count));
                        millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
                    }

                    if (millisUntilFinished > DateUtils.HOUR_IN_MILLIS) {
                        long count = millisUntilFinished / DateUtils.HOUR_IN_MILLIS;
                        CountDownHoursTextView.setText(String.valueOf(count));
                        millisUntilFinished %= DateUtils.HOUR_IN_MILLIS;
                    }

                    if (millisUntilFinished > DateUtils.MINUTE_IN_MILLIS) {
                        long count = millisUntilFinished / DateUtils.MINUTE_IN_MILLIS;
                        CountDownMinutesTextView.setText(String.valueOf(count));
                        millisUntilFinished %= DateUtils.MINUTE_IN_MILLIS;
                    }

                    if (millisUntilFinished > DateUtils.SECOND_IN_MILLIS) {
                        long count = millisUntilFinished / DateUtils.SECOND_IN_MILLIS;
                        CountDownSecondsTextView.setText(String.valueOf(count));
                    }
                }

                public void onFinish() {
                    CountDownImageView.setImageResource(R.drawable.big_day);
                }
            }.start();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }
}
